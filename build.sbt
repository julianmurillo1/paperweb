
name := """PaperWeb"""
organization := "gtres"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava,PlayEbean)
//lazy val myProject = (project in file(".")).enablePlugins(PlayJava, PlayEbean)
scalaVersion := "2.12.2"
herokuAppName in Compile := "paperwebapp"
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
libraryDependencies ++= Seq(evolutions, jdbc)

libraryDependencies += jdbc
libraryDependencies += guice
libraryDependencies += javaJdbc
libraryDependencies += javaWs

// Test Database
libraryDependencies += "com.h2database" % "h2" % "1.4.194"

// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.41"
// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))

libraryDependencies ++= Seq( "org.scalatestplus.play" %% "scalatestplus-play" % "x.x.x" % "test" )

