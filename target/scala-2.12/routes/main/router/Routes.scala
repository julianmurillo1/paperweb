
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/julianmurillo/Documents/programming/PlayFramework/paperweb/conf/routes
// @DATE:Fri Dec 01 19:57:04 COT 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:10
  Assets_1: controllers.Assets,
  // @LINE:13
  ProductsController_2: controllers.ProductsController,
  // @LINE:26
  WarehousesController_3: controllers.WarehousesController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:10
    Assets_1: controllers.Assets,
    // @LINE:13
    ProductsController_2: controllers.ProductsController,
    // @LINE:26
    WarehousesController_3: controllers.WarehousesController
  ) = this(errorHandler, HomeController_0, Assets_1, ProductsController_2, WarehousesController_3, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, Assets_1, ProductsController_2, WarehousesController_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """controllers.ProductsController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/create""", """controllers.ProductsController.create()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """controllers.ProductsController.show(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/edit/""" + "$" + """id<[^/]+>""", """controllers.ProductsController.edit(id:Integer)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/edit""", """controllers.ProductsController.update()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/create""", """controllers.ProductsController.save()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/delete/""" + "$" + """id<[^/]+>""", """controllers.ProductsController.destroy(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses""", """controllers.WarehousesController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/create""", """controllers.WarehousesController.create()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/""" + "$" + """id<[^/]+>""", """controllers.WarehousesController.show(id:Integer)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/create""", """controllers.WarehousesController.save()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/edit/""" + "$" + """id<[^/]+>""", """controllers.WarehousesController.edit(id:Integer)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/edit""", """controllers.WarehousesController.update()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """warehouses/delete/""" + "$" + """id<[^/]+>""", """controllers.WarehousesController.destroy(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """inventory""", """controllers.ProductsController.inventory()"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:13
  private[this] lazy val controllers_ProductsController_index2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_ProductsController_index2_invoker = createInvoker(
    ProductsController_2.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "index",
      Nil,
      "GET",
      this.prefix + """products""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_ProductsController_create3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/create")))
  )
  private[this] lazy val controllers_ProductsController_create3_invoker = createInvoker(
    ProductsController_2.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "create",
      Nil,
      "GET",
      this.prefix + """products/create""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_ProductsController_show4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductsController_show4_invoker = createInvoker(
    ProductsController_2.show(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "show",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_ProductsController_edit5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/edit/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductsController_edit5_invoker = createInvoker(
    ProductsController_2.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """products/edit/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_ProductsController_update6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/edit")))
  )
  private[this] lazy val controllers_ProductsController_update6_invoker = createInvoker(
    ProductsController_2.update(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "update",
      Nil,
      "POST",
      this.prefix + """products/edit""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:20
  private[this] lazy val controllers_ProductsController_save7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/create")))
  )
  private[this] lazy val controllers_ProductsController_save7_invoker = createInvoker(
    ProductsController_2.save(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "save",
      Nil,
      "POST",
      this.prefix + """products/create""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:23
  private[this] lazy val controllers_ProductsController_destroy8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductsController_destroy8_invoker = createInvoker(
    ProductsController_2.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """products/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_WarehousesController_index9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses")))
  )
  private[this] lazy val controllers_WarehousesController_index9_invoker = createInvoker(
    WarehousesController_3.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "index",
      Nil,
      "GET",
      this.prefix + """warehouses""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_WarehousesController_create10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/create")))
  )
  private[this] lazy val controllers_WarehousesController_create10_invoker = createInvoker(
    WarehousesController_3.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "create",
      Nil,
      "GET",
      this.prefix + """warehouses/create""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_WarehousesController_show11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WarehousesController_show11_invoker = createInvoker(
    WarehousesController_3.show(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "show",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """warehouses/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_WarehousesController_save12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/create")))
  )
  private[this] lazy val controllers_WarehousesController_save12_invoker = createInvoker(
    WarehousesController_3.save(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "save",
      Nil,
      "POST",
      this.prefix + """warehouses/create""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:31
  private[this] lazy val controllers_WarehousesController_edit13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/edit/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WarehousesController_edit13_invoker = createInvoker(
    WarehousesController_3.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """warehouses/edit/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:33
  private[this] lazy val controllers_WarehousesController_update14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/edit")))
  )
  private[this] lazy val controllers_WarehousesController_update14_invoker = createInvoker(
    WarehousesController_3.update(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "update",
      Nil,
      "POST",
      this.prefix + """warehouses/edit""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:36
  private[this] lazy val controllers_WarehousesController_destroy15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("warehouses/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WarehousesController_destroy15_invoker = createInvoker(
    WarehousesController_3.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WarehousesController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """warehouses/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_ProductsController_inventory16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("inventory")))
  )
  private[this] lazy val controllers_ProductsController_inventory16_invoker = createInvoker(
    ProductsController_2.inventory(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductsController",
      "inventory",
      Nil,
      "GET",
      this.prefix + """inventory""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:10
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_1.versioned(path, file))
      }
  
    // @LINE:13
    case controllers_ProductsController_index2_route(params@_) =>
      call { 
        controllers_ProductsController_index2_invoker.call(ProductsController_2.index())
      }
  
    // @LINE:14
    case controllers_ProductsController_create3_route(params@_) =>
      call { 
        controllers_ProductsController_create3_invoker.call(ProductsController_2.create())
      }
  
    // @LINE:15
    case controllers_ProductsController_show4_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_ProductsController_show4_invoker.call(ProductsController_2.show(id))
      }
  
    // @LINE:16
    case controllers_ProductsController_edit5_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_ProductsController_edit5_invoker.call(ProductsController_2.edit(id))
      }
  
    // @LINE:18
    case controllers_ProductsController_update6_route(params@_) =>
      call { 
        controllers_ProductsController_update6_invoker.call(ProductsController_2.update())
      }
  
    // @LINE:20
    case controllers_ProductsController_save7_route(params@_) =>
      call { 
        controllers_ProductsController_save7_invoker.call(ProductsController_2.save())
      }
  
    // @LINE:23
    case controllers_ProductsController_destroy8_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_ProductsController_destroy8_invoker.call(ProductsController_2.destroy(id))
      }
  
    // @LINE:26
    case controllers_WarehousesController_index9_route(params@_) =>
      call { 
        controllers_WarehousesController_index9_invoker.call(WarehousesController_3.index())
      }
  
    // @LINE:27
    case controllers_WarehousesController_create10_route(params@_) =>
      call { 
        controllers_WarehousesController_create10_invoker.call(WarehousesController_3.create())
      }
  
    // @LINE:28
    case controllers_WarehousesController_show11_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_WarehousesController_show11_invoker.call(WarehousesController_3.show(id))
      }
  
    // @LINE:30
    case controllers_WarehousesController_save12_route(params@_) =>
      call { 
        controllers_WarehousesController_save12_invoker.call(WarehousesController_3.save())
      }
  
    // @LINE:31
    case controllers_WarehousesController_edit13_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_WarehousesController_edit13_invoker.call(WarehousesController_3.edit(id))
      }
  
    // @LINE:33
    case controllers_WarehousesController_update14_route(params@_) =>
      call { 
        controllers_WarehousesController_update14_invoker.call(WarehousesController_3.update())
      }
  
    // @LINE:36
    case controllers_WarehousesController_destroy15_route(params@_) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_WarehousesController_destroy15_invoker.call(WarehousesController_3.destroy(id))
      }
  
    // @LINE:38
    case controllers_ProductsController_inventory16_route(params@_) =>
      call { 
        controllers_ProductsController_inventory16_invoker.call(ProductsController_2.inventory())
      }
  }
}
