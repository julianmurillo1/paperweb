
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/julianmurillo/Documents/programming/PlayFramework/paperweb/conf/routes
// @DATE:Fri Dec 01 19:57:04 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
