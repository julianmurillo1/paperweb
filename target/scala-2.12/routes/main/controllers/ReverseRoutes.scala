
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/julianmurillo/Documents/programming/PlayFramework/paperweb/conf/routes
// @DATE:Fri Dec 01 19:57:04 COT 2017

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:13
  class ReverseProductsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def edit(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:38
    def inventory(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "inventory")
    }
  
    // @LINE:14
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/create")
    }
  
    // @LINE:15
    def show(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:23
    def destroy(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:20
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "products/create")
    }
  
    // @LINE:18
    def update(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "products/edit")
    }
  
    // @LINE:13
    def index(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products")
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:10
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:26
  class ReverseWarehousesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:31
    def edit(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "warehouses/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:27
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "warehouses/create")
    }
  
    // @LINE:28
    def show(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "warehouses/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:36
    def destroy(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "warehouses/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("id", id)))
    }
  
    // @LINE:30
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "warehouses/create")
    }
  
    // @LINE:33
    def update(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "warehouses/edit")
    }
  
    // @LINE:26
    def index(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "warehouses")
    }
  
  }


}
