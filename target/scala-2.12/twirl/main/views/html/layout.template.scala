
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object layout extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title :String)(body:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.28*/("""
"""),format.raw/*2.1*/("""<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>"""),_display_(/*7.17*/title),format.raw/*7.22*/("""</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
            <!-- Bootstrap CSS-->
        <link rel="stylesheet" href=""""),_display_(/*12.39*/routes/*12.45*/.Assets.versioned("vendor/bootstrap/css/bootstrap.min.css")),format.raw/*12.104*/("""">
            <!-- Font Awesome CSS-->
        <link rel="stylesheet" href=""""),_display_(/*14.39*/routes/*14.45*/.Assets.versioned("vendor/font-awesome/css/font-awesome.min.css")),format.raw/*14.110*/("""">
            <!-- Custom Font Icons CSS-->
        <link rel="stylesheet" href=""""),_display_(/*16.39*/routes/*16.45*/.Assets.versioned("css/fontastic.css")),format.raw/*16.83*/("""">
            <!-- Google fonts - Muli-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
            <!-- theme stylesheet-->
        <link rel="stylesheet" href=""""),_display_(/*20.39*/routes/*20.45*/.Assets.versioned("css/style.default.css" )),format.raw/*20.88*/("""" id="theme-stylesheet">
            <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href=""""),_display_(/*22.39*/routes/*22.45*/.Assets.versioned("css/style.blue.css")),format.raw/*22.84*/("""">
            <!-- Favicon-->
      <link rel="shortcut icon" href=""""),_display_(/*24.40*/routes/*24.46*/.Assets.versioned("img/favicon.ico")),format.raw/*24.82*/("""">
    </head>


    <body>
        <div class="page home-page">
                <!-- Main Navbar-->
            <header class="header">
                <nav class="navbar">

                    <div class="container-fluid">
                        <div class="navbar-holder d-flex align-items-center justify-content-between">
                                <!-- Navbar Header-->
                            <div class="navbar-header">
                                    <!-- Navbar Brand --><a href="/" class="navbar-brand">
                                <div class="brand-text brand-big"><span>Paper </span><strong>Web</strong></div>
                                <div class="brand-text brand-small"><strong>PW</strong></div></a>
                                    <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                            </div>

                        </div>
                    </div>
                </nav>
            </header>
            <div class="page-content d-flex align-items-stretch">
                    <!-- Side Navbar -->
                <nav class="side-navbar">
                        <!-- Sidebar Header-->
                    <div class="sidebar-header d-flex align-items-center">
                        <div class="avatar"><img src=""""),_display_(/*53.56*/routes/*53.62*/.Assets.versioned("img/Admin.png")),format.raw/*53.96*/("""" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="title">
                            <h1 class="h4">Nombre Admin</h1>
                            <p>Administrador</p>
                        </div>
                    </div>
                        <!-- Sidebar Navidation Menus--><span class="heading">Menú</span>
                    <ul class="list-unstyled">
                        <li class=""> <a href="/"><i class="icon-home"></i>Inicio</a></li>
                        <li><a href="#dashvariants" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Bodegas </a>
                            <ul id="dashvariants" class="collapse list-unstyled">
                                <li><a href="/warehouses">Lista bodegas</a></li>
                                <li><a href="/warehouses/create">Crear nueva</a></li>

                            </ul>
                        </li>
                        <li><a href="#dashvariants2" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Productos </a>
                            <ul id="dashvariants2" class="collapse list-unstyled">
                                <li><a href="/products">Lista productos</a></li>
                                <li><a href="/products/create">Crear nuevo</a></li>

                            </ul>
                        </li>
                        <li> <a href="/inventory"> <i class="icon-padnote"></i>Reporte de inventario </a></li>

                    </ul>
                </nav>
                <div class="content-inner">
                        <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Dashboard</h2>
                        </div>
                    </header>
                        <!-- Dashboard Counts Section-->
                    <section class="dashboard-counts forms " style="text-align:center">
                        <div class="container-fluid ">
                            <div class="row bg-white has-shadow">
                                    """),_display_(/*91.38*/body),format.raw/*91.42*/("""


                            """),format.raw/*94.29*/("""</div>
                        </div>
                    </section>


                        <!-- Page Footer-->
                    <footer class="main-footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>PaperWeb &copy; 2017-2019</p>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                                        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>

        <script src=""""),_display_(/*117.23*/routes/*117.29*/.Assets.versioned("js/jquery-3.2.1.min.js")),format.raw/*117.72*/(""""></script>
        <script src=""""),_display_(/*118.23*/routes/*118.29*/.Assets.versioned("js/popper.min.js")),format.raw/*118.66*/(""""> </script>
        <script src=""""),_display_(/*119.23*/routes/*119.29*/.Assets.versioned("vendor/bootstrap/js/bootstrap.min.js")),format.raw/*119.86*/(""""></script>
        <script src=""""),_display_(/*120.23*/routes/*120.29*/.Assets.versioned("vendor/jquery.cookie/jquery.cookie.js")),format.raw/*120.87*/(""""> </script>
        <script src=""""),_display_(/*121.23*/routes/*121.29*/.Assets.versioned("vendor/chart.js/Chart.min.js")),format.raw/*121.78*/(""""></script>
        <script src=""""),_display_(/*122.23*/routes/*122.29*/.Assets.versioned("vendor/jquery-validation/jquery.validate.min.js")),format.raw/*122.97*/(""""></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
         <script src=""""),_display_(/*125.24*/routes/*125.30*/.Assets.versioned("js/charts-home.js")),format.raw/*125.68*/(""""></script>
        <script src=""""),_display_(/*126.23*/routes/*126.29*/.Assets.versioned("js/front.js")),format.raw/*126.61*/(""""></script>


    </body>
</html>"""))
      }
    }
  }

  def render(title:String,body:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(body)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (body) => apply(title)(body)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 03:25:30 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/layout.scala.html
                  HASH: d41bc4507e874266b437812e96b30f9f9d1f0a46
                  MATRIX: 954->1|1075->27|1102->28|1271->171|1296->176|1576->429|1591->435|1672->494|1777->572|1792->578|1879->643|1989->726|2004->732|2063->770|2304->984|2319->990|2383->1033|2530->1153|2545->1159|2605->1198|2702->1268|2717->1274|2774->1310|4157->2666|4172->2672|4227->2706|6462->4914|6487->4918|6546->4949|7663->6038|7679->6044|7744->6087|7806->6121|7822->6127|7881->6164|7944->6199|7960->6205|8039->6262|8101->6296|8117->6302|8197->6360|8260->6395|8276->6401|8347->6450|8409->6484|8425->6490|8515->6558|8678->6693|8694->6699|8754->6737|8816->6771|8832->6777|8886->6809
                  LINES: 28->1|33->1|34->2|39->7|39->7|44->12|44->12|44->12|46->14|46->14|46->14|48->16|48->16|48->16|52->20|52->20|52->20|54->22|54->22|54->22|56->24|56->24|56->24|85->53|85->53|85->53|123->91|123->91|126->94|149->117|149->117|149->117|150->118|150->118|150->118|151->119|151->119|151->119|152->120|152->120|152->120|153->121|153->121|153->121|154->122|154->122|154->122|157->125|157->125|157->125|158->126|158->126|158->126
                  -- GENERATED --
              */
          