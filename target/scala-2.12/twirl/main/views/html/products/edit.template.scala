
package views.html.products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object edit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Form[Product],List[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ProductForm: Form[Product], warehouses: List[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.3*/import helper._


Seq[Any](format.raw/*1.59*/("""
 """),format.raw/*3.1*/("""
 """),_display_(/*4.3*/layout("Editar producto")/*4.28*/{_display_(Seq[Any](format.raw/*4.29*/("""
        """),format.raw/*5.9*/("""<h1>Editar producto</h1>
    <div class="col-lg-6">
        <div class="block">
            <div class="block-body">
            """),_display_(/*9.14*/helper/*9.20*/.form(action= routes.ProductsController.update(), 'enctype -> "multipart/form-data")/*9.104*/{_display_(Seq[Any](format.raw/*9.105*/("""
                """),format.raw/*10.17*/("""<div class="form-group row">
                    <label class="col-sm-3 form-control-label">Id</label>
                    <div class="col-sm-9">
                    """),_display_(/*13.22*/helper/*13.28*/.inputText(ProductForm("id"),'class->"form-control",'_label ->"",'placeholder->"id",'readonly->"readonly")),format.raw/*13.134*/("""
                    """),format.raw/*14.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Código</label>
                    <div class="col-sm-9">
                    """),_display_(/*19.22*/helper/*19.28*/.inputText(ProductForm("code"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Codigo",'max->"9999999",'type->"number",'required->"true" )),format.raw/*19.190*/("""
                    """),format.raw/*20.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Nombre</label>
                    <div class="col-sm-9">
                    """),_display_(/*25.22*/helper/*25.28*/.inputText(ProductForm("name"),'class->"form-control",'_label ->"",'placeholder->"Nombre",'required->"true")),format.raw/*25.136*/("""
                    """),format.raw/*26.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Marca</label>
                    <div class="col-sm-9">
                    """),_display_(/*31.22*/helper/*31.28*/.inputText(ProductForm("brand"),'class->"form-control",'_label ->"",'placeholder->"Marca")),format.raw/*31.118*/("""
                    """),format.raw/*32.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Proveedor</label>
                    <div class="col-sm-9">
                    """),_display_(/*37.22*/helper/*37.28*/.inputText(ProductForm("provider"),'class->"form-control",'_label ->"",'placeholder->"Proveedor")),format.raw/*37.125*/("""
                    """),format.raw/*38.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Numero de productos</label>
                    <div class="col-sm-9">
                    """),_display_(/*43.22*/helper/*43.28*/.inputText(ProductForm("stock"),'class->"form-control",'_label ->"",'placeholder->"Numero de inventario",'max->"9999999",'type->"number",'required->"true")),format.raw/*43.183*/("""
                    """),format.raw/*44.21*/("""</div>
                </div>

                <label class="col-sm-3 form-control-label">Seleccione la bodega</label>
                <div class="form-group col-sm-9 select">

                    <select  name="warehouse" class="form-control">
                    """),_display_(/*51.22*/for(warehouse <- warehouses) yield /*51.50*/ {_display_(Seq[Any](format.raw/*51.52*/("""
                        """),format.raw/*52.25*/("""<option  value=""""),_display_(/*52.42*/warehouse/*52.51*/.id),format.raw/*52.54*/("""">"""),_display_(/*52.57*/warehouse/*52.66*/.id),format.raw/*52.69*/(""" """),format.raw/*52.70*/("""- """),_display_(/*52.73*/warehouse/*52.82*/.name),format.raw/*52.87*/("""</option>
                    """)))}),format.raw/*53.22*/("""
                    """),format.raw/*54.21*/("""</select>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Subir imagen de producto</label>
                    <div class="col-sm-9">
                    """),_display_(/*60.22*/helper/*60.28*/.inputFile(ProductForm("file"),'class->"custom-file",'_label ->"",'placeholder->"Imagen")),format.raw/*60.117*/("""

                    """),format.raw/*62.21*/("""</div>
                </div>

                <button type="submit" class="btn btn-success btn-block">Editar</button>

            """)))}),format.raw/*67.14*/("""
            """),format.raw/*68.13*/("""</div>
        </div>
    </div>

""")))}),format.raw/*72.2*/("""
"""))
      }
    }
  }

  def render(ProductForm:Form[Product],warehouses:List[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(ProductForm,warehouses)

  def f:((Form[Product],List[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (ProductForm,warehouses) => apply(ProductForm,warehouses)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 20:01:18 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/products/edit.scala.html
                  HASH: cdf24305526ad0a69ed71ad76a752c9672be45bb
                  MATRIX: 979->1|1109->61|1154->58|1182->77|1210->80|1243->105|1281->106|1316->115|1472->245|1486->251|1579->335|1618->336|1663->353|1857->520|1872->526|2000->632|2049->653|2293->870|2308->876|2492->1038|2541->1059|2785->1276|2800->1282|2930->1390|2979->1411|3222->1627|3237->1633|3349->1723|3398->1744|3645->1964|3660->1970|3779->2067|3828->2088|4085->2318|4100->2324|4277->2479|4326->2500|4619->2766|4663->2794|4703->2796|4756->2821|4800->2838|4818->2847|4842->2850|4872->2853|4890->2862|4914->2865|4943->2866|4973->2869|4991->2878|5017->2883|5079->2914|5128->2935|5394->3174|5409->3180|5520->3269|5570->3291|5734->3424|5775->3437|5840->3472
                  LINES: 28->1|31->2|34->1|35->3|36->4|36->4|36->4|37->5|41->9|41->9|41->9|41->9|42->10|45->13|45->13|45->13|46->14|51->19|51->19|51->19|52->20|57->25|57->25|57->25|58->26|63->31|63->31|63->31|64->32|69->37|69->37|69->37|70->38|75->43|75->43|75->43|76->44|83->51|83->51|83->51|84->52|84->52|84->52|84->52|84->52|84->52|84->52|84->52|84->52|84->52|84->52|85->53|86->54|92->60|92->60|92->60|94->62|99->67|100->68|104->72
                  -- GENERATED --
              */
          