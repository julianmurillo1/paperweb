
package views.html.products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object create extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Form[Product],List[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ProductForm: Form[Product] , warehouses: List[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*1.60*/("""

"""),_display_(/*4.2*/layout("Crear Producto")/*4.26*/{_display_(Seq[Any](format.raw/*4.27*/("""
    """),format.raw/*5.5*/("""<h1>Crear Nuevo Producto</h1>


    <div class="col-lg-12">
        <div class="block">

            <div class="block-body">


                """),_display_(/*14.18*/helper/*14.24*/.form(action= routes.ProductsController.save(), 'enctype -> "multipart/form-data")/*14.106*/{_display_(Seq[Any](format.raw/*14.107*/("""

        """),format.raw/*16.9*/("""<div class="form-group row">
            <label class="col-sm-3 form-control-label">Código</label>
            <div class="col-sm-9">
            """),_display_(/*19.14*/helper/*19.20*/.inputText(ProductForm("code"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Codigo",'max->"99999999",'type->"number",'required->"true" )),format.raw/*19.183*/("""
            """),format.raw/*20.13*/("""</div>
        </div>
        <div class="form-group row">
             <label class="col-sm-3 form-control-label">Nombre</label>
              <div class="col-sm-9">
               """),_display_(/*25.17*/helper/*25.23*/.inputText(ProductForm("name"),'class->"form-control",'_label ->"",'placeholder->"Nombre",'required->"true")),format.raw/*25.131*/("""
              """),format.raw/*26.15*/("""</div>
        </div>
        <div class="form-group row">
              <label class="col-sm-3 form-control-label">Marca</label>
               <div class="col-sm-9">
               """),_display_(/*31.17*/helper/*31.23*/.inputText(ProductForm("brand"),'class->"form-control",'_label ->"",'placeholder->"Marca")),format.raw/*31.113*/("""
            """),format.raw/*32.13*/("""</div>
        </div>
         <div class="form-group row">
              <label class="col-sm-3 form-control-label">Proveedor</label>
              <div class="col-sm-9">
              """),_display_(/*37.16*/helper/*37.22*/.inputText(ProductForm("provider"),'class->"form-control",'_label ->"",'placeholder->"Proveedor")),format.raw/*37.119*/("""
            """),format.raw/*38.13*/("""</div>
         </div>
        <div class="form-group row">
               <label class="col-sm-3 form-control-label">Numero de productos</label>
               <div class="col-sm-9">
               """),_display_(/*43.17*/helper/*43.23*/.inputText(ProductForm("stock"),'class->"form-control",'_label ->"",'placeholder->"Numero de inventario",'max->"99999999",'type->"number",'required->"true")),format.raw/*43.179*/("""

               """),format.raw/*45.16*/("""</div>
          </div>
                    <div class="form-group row">

                        <label class="col-sm-3 form-control-label">Seleccione la bodega</label>
                        <div class="form-group col-sm-9 select">

                                <select  name="warehouse" class="form-control">
                                """),_display_(/*53.34*/for(warehouse <- warehouses) yield /*53.62*/ {_display_(Seq[Any](format.raw/*53.64*/("""
                                    """),format.raw/*54.37*/("""<option  value=""""),_display_(/*54.54*/warehouse/*54.63*/.id),format.raw/*54.66*/("""">"""),_display_(/*54.69*/warehouse/*54.78*/.id),format.raw/*54.81*/(""" """),format.raw/*54.82*/("""- """),_display_(/*54.85*/warehouse/*54.94*/.name),format.raw/*54.99*/("""</option>
                                """)))}),format.raw/*55.34*/("""
                            """),format.raw/*56.29*/("""</select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Subir imagen de producto</label>
                        <div class="col-sm-9">
                        """),_display_(/*64.26*/helper/*64.32*/.inputFile(ProductForm("file"),'class->"custom-file",'_label ->"",'placeholder->"Imagen")),format.raw/*64.121*/("""

                        """),format.raw/*66.25*/("""</div>
                    </div>

                 <button type="submit" class="btn btn-success btn-block">Crear</button>

                """)))}),format.raw/*71.18*/("""
            """),format.raw/*72.13*/("""</div>
        </div>
    </div>
    </





""")))}))
      }
    }
  }

  def render(ProductForm:Form[Product],warehouses:List[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(ProductForm,warehouses)

  def f:((Form[Product],List[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (ProductForm,warehouses) => apply(ProductForm,warehouses)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 20:01:18 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/products/create.scala.html
                  HASH: 20bb5c5b3c51b3583862b05f78cdbe226705007e
                  MATRIX: 981->1|1112->62|1157->59|1185->79|1217->103|1255->104|1286->109|1458->254|1473->260|1565->342|1605->343|1642->353|1816->500|1831->506|2016->669|2057->682|2267->865|2282->871|2412->979|2455->994|2666->1178|2681->1184|2793->1274|2834->1287|3048->1474|3063->1480|3182->1577|3223->1590|3450->1790|3465->1796|3643->1952|3688->1969|4064->2318|4108->2346|4148->2348|4213->2385|4257->2402|4275->2411|4299->2414|4329->2417|4347->2426|4371->2429|4400->2430|4430->2433|4448->2442|4474->2447|4548->2490|4605->2519|4923->2810|4938->2816|5049->2905|5103->2931|5275->3072|5316->3085
                  LINES: 28->1|31->3|34->1|36->4|36->4|36->4|37->5|46->14|46->14|46->14|46->14|48->16|51->19|51->19|51->19|52->20|57->25|57->25|57->25|58->26|63->31|63->31|63->31|64->32|69->37|69->37|69->37|70->38|75->43|75->43|75->43|77->45|85->53|85->53|85->53|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|87->55|88->56|96->64|96->64|96->64|98->66|103->71|104->72
                  -- GENERATED --
              */
          