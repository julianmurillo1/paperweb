
package views.html.products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(products: List[Product]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.27*/("""

"""),_display_(/*3.2*/layout("Productos")/*3.21*/{_display_(Seq[Any](format.raw/*3.22*/("""
    """),format.raw/*4.5*/("""<div class="container-fluid">

        <div class="row">
            <div class="col-lg-1">
            </div>

            <div class="col-lg-9">
                <div class="block margin-bottom-sm">

                    """),_display_(/*13.22*/if( products.length != 0)/*13.47*/ {_display_(Seq[Any](format.raw/*13.49*/("""
                    """),format.raw/*14.21*/("""<div class="card-header d-flex align-items-center">
                        <h3 class="h4">Productos</h3>
                    </div>

                    <table class="table    table-hover table-responsive">
        <thead >
            <tr>
                <th scope="col">Código </th>
                <th scope="col">Nombre</th>
                <th scope="col">Marca</th>
                <th scope="col">Proveedor</th>
                <th scope="col">Numero Unidades</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        """),_display_(/*32.10*/for(product <- products) yield /*32.34*/ {_display_(Seq[Any](format.raw/*32.36*/("""
            """),_display_(/*33.14*/if(product.stock == 0)/*33.36*/ {_display_(Seq[Any](format.raw/*33.38*/("""
            """),format.raw/*34.13*/("""<tr class="alert alert-danger" role="alert">



                    <td >"""),_display_(/*38.27*/product/*38.34*/.code),format.raw/*38.39*/("""</td>
                    <td>"""),_display_(/*39.26*/product/*39.33*/.name),format.raw/*39.38*/("""</td>
                    <td>"""),_display_(/*40.26*/product/*40.33*/.brand),format.raw/*40.39*/("""</td>
                    <td>"""),_display_(/*41.26*/product/*41.33*/.provider),format.raw/*41.42*/("""</td>
                    <td>"""),_display_(/*42.26*/product/*42.33*/.stock),format.raw/*42.39*/("""</td>
                    <td><a href="/products/"""),_display_(/*43.45*/product/*43.52*/.id),format.raw/*43.55*/("""" class="btn btn-secondary">Detalles </a></td>
                    <td><a href="/products/edit/"""),_display_(/*44.50*/product/*44.57*/.id),format.raw/*44.60*/("""" class="btn btn-success">Editar </a></td>
                    <td><a href=""""),_display_(/*45.35*/routes/*45.41*/.ProductsController.destroy(product.id)),format.raw/*45.80*/("""" class="btn btn-danger">Eliminar</a></td>



                """)))}/*49.18*/else/*49.23*/{_display_(Seq[Any](format.raw/*49.24*/("""

                    """),format.raw/*51.21*/("""<th scope="row">"""),_display_(/*51.38*/product/*51.45*/.code),format.raw/*51.50*/("""</th>
                    <td>"""),_display_(/*52.26*/product/*52.33*/.name),format.raw/*52.38*/("""</td>
                    <td>"""),_display_(/*53.26*/product/*53.33*/.brand),format.raw/*53.39*/("""</td>
                    <td>"""),_display_(/*54.26*/product/*54.33*/.provider),format.raw/*54.42*/("""</td>
                    <td>"""),_display_(/*55.26*/product/*55.33*/.stock),format.raw/*55.39*/("""</td>
                    <td><a href="/products/"""),_display_(/*56.45*/product/*56.52*/.id),format.raw/*56.55*/("""" class="btn btn-secondary">Detalles </a></td>
                    <td><a href="/products/edit/"""),_display_(/*57.50*/product/*57.57*/.id),format.raw/*57.60*/("""" class="btn btn-success">Editar </a></td>
                    <td><a href=""""),_display_(/*58.35*/routes/*58.41*/.ProductsController.destroy(product.id)),format.raw/*58.80*/("""" class="btn btn-danger">Eliminar</a></td>

                """)))}),format.raw/*60.18*/("""
            """),format.raw/*61.13*/("""</tr>
        """)))}),format.raw/*62.10*/("""
        """),format.raw/*63.9*/("""</tbody>
                    </table>
                    """)))}/*65.22*/else/*65.27*/{_display_(Seq[Any](format.raw/*65.28*/("""
                        """),format.raw/*66.25*/("""<div class="card-header  align-items-center " style="width: 100%">
                            <h3 class="h4">  No hay productos para mostrar
                            </h3>
                        </div>
                    """)))}),format.raw/*70.22*/("""
                """),format.raw/*71.17*/("""</div>
            </div>
        </div>
    </div>







""")))}),format.raw/*82.2*/("""
"""))
      }
    }
  }

  def render(products:List[Product]): play.twirl.api.HtmlFormat.Appendable = apply(products)

  def f:((List[Product]) => play.twirl.api.HtmlFormat.Appendable) = (products) => apply(products)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 19:17:13 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/products/index.scala.html
                  HASH: 6b847145f21241d9ac90cf4b4265bae4379404df
                  MATRIX: 964->1|1084->26|1112->29|1139->48|1177->49|1208->54|1457->276|1491->301|1531->303|1580->324|2219->936|2259->960|2299->962|2340->976|2371->998|2411->1000|2452->1013|2553->1087|2569->1094|2595->1099|2653->1130|2669->1137|2695->1142|2753->1173|2769->1180|2796->1186|2854->1217|2870->1224|2900->1233|2958->1264|2974->1271|3001->1277|3078->1327|3094->1334|3118->1337|3241->1433|3257->1440|3281->1443|3385->1520|3400->1526|3460->1565|3542->1628|3555->1633|3594->1634|3644->1656|3688->1673|3704->1680|3730->1685|3788->1716|3804->1723|3830->1728|3888->1759|3904->1766|3931->1772|3989->1803|4005->1810|4035->1819|4093->1850|4109->1857|4136->1863|4213->1913|4229->1920|4253->1923|4376->2019|4392->2026|4416->2029|4520->2106|4535->2112|4595->2151|4687->2212|4728->2225|4774->2240|4810->2249|4888->2308|4901->2313|4940->2314|4993->2339|5252->2567|5297->2584|5387->2644
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|45->13|45->13|45->13|46->14|64->32|64->32|64->32|65->33|65->33|65->33|66->34|70->38|70->38|70->38|71->39|71->39|71->39|72->40|72->40|72->40|73->41|73->41|73->41|74->42|74->42|74->42|75->43|75->43|75->43|76->44|76->44|76->44|77->45|77->45|77->45|81->49|81->49|81->49|83->51|83->51|83->51|83->51|84->52|84->52|84->52|85->53|85->53|85->53|86->54|86->54|86->54|87->55|87->55|87->55|88->56|88->56|88->56|89->57|89->57|89->57|90->58|90->58|90->58|92->60|93->61|94->62|95->63|97->65|97->65|97->65|98->66|102->70|103->71|114->82
                  -- GENERATED --
              */
          