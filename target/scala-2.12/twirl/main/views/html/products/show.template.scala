
package views.html.products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object show extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Product,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(product: Product):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.20*/("""
"""),_display_(/*2.2*/layout(product.name)/*2.22*/{_display_(Seq[Any](format.raw/*2.23*/("""
   """),format.raw/*3.4*/("""<div class="container-fluid text-secondary">


       """),_display_(/*6.9*/if(product.stock ==0)/*6.30*/{_display_(Seq[Any](format.raw/*6.31*/("""
           """),format.raw/*7.12*/("""<div class="p-3 mb-2 bg-danger text-white">
                  Este producto se encuentra agotado en stock
           </div>
       """)))}),format.raw/*10.9*/("""

      """),format.raw/*12.7*/("""<div class="row">
<div>
    <h1 class="text-primary">    """),_display_(/*14.35*/product/*14.42*/.name),format.raw/*14.47*/("""   """),format.raw/*14.50*/("""</h1>
</div>


          <div class="col-lg-3">
         <img src="""),_display_(/*19.20*/routes/*19.26*/.Assets.versioned("images/products/")),_display_(/*19.64*/product/*19.71*/.image),format.raw/*19.77*/(""">
          </div>
         <div class="col-lg-6"><strong class="text-secondary">Detalles</strong>
             <br>
            <ul class="list-unstyled"><br>
               <li>Marca : """),_display_(/*24.29*/product/*24.36*/.brand),format.raw/*24.42*/(""" """),format.raw/*24.43*/("""</li><br>
               <li> Proveedor : """),_display_(/*25.34*/product/*25.41*/.provider),format.raw/*25.50*/("""  """),format.raw/*25.52*/("""</li><br>
               <li>Cantidad en stock : """),_display_(/*26.41*/product/*26.48*/.stock),format.raw/*26.54*/("""</li><br>
               <li>Fecha Actualizacion : """),_display_(/*27.43*/product/*27.50*/.date.format("dd/MM/YYYY - HH:mm")),format.raw/*27.84*/("""</li><br>

            </ul>

             <a href="/products/edit/"""),_display_(/*31.39*/product/*31.46*/.id),format.raw/*31.49*/("""" class="btn btn-success">Editar </a>
         </div>




      </div>
   </div>
""")))}))
      }
    }
  }

  def render(product:Product): play.twirl.api.HtmlFormat.Appendable = apply(product)

  def f:((Product) => play.twirl.api.HtmlFormat.Appendable) = (product) => apply(product)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 19:55:15 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/products/show.scala.html
                  HASH: 4d6a5e03d0c31852b2508c429342974b66db9750
                  MATRIX: 957->1|1070->19|1097->21|1125->41|1163->42|1193->46|1273->101|1302->122|1340->123|1379->135|1541->267|1576->275|1661->333|1677->340|1703->345|1734->348|1828->415|1843->421|1901->459|1917->466|1944->472|2159->660|2175->667|2202->673|2231->674|2301->717|2317->724|2347->733|2377->735|2454->785|2470->792|2497->798|2576->850|2592->857|2647->891|2742->959|2758->966|2782->969
                  LINES: 28->1|33->1|34->2|34->2|34->2|35->3|38->6|38->6|38->6|39->7|42->10|44->12|46->14|46->14|46->14|46->14|51->19|51->19|51->19|51->19|51->19|56->24|56->24|56->24|56->24|57->25|57->25|57->25|57->25|58->26|58->26|58->26|59->27|59->27|59->27|63->31|63->31|63->31
                  -- GENERATED --
              */
          