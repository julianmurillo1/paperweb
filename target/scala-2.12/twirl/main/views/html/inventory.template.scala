
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object inventory extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[List[Product],List[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(products : List[Product], warehouses : List[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.58*/("""


"""),_display_(/*4.2*/layout("Paper Web")/*4.21*/{_display_(Seq[Any](format.raw/*4.22*/("""
"""),format.raw/*5.1*/("""<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6">
            <div class="block margin-bottom-sm">
            """),_display_(/*9.14*/if(products.length != 0)/*9.38*/ {_display_(Seq[Any](format.raw/*9.40*/("""

                """),format.raw/*11.17*/("""<div class="card-header d-flex align-items-center">

                    <h3 class="h4">Inventario</h3>
                </div>
                <table class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th scope="col">Código </th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Numero Unidades en inventario</th>
                            <th scope="col">Bodega</th>
                            <th scope="col">Fecha actualización</th>
                        </tr>
                    </thead>
                    <tbody>


                    """),_display_(/*28.22*/for(warehouse <- warehouses) yield /*28.50*/ {_display_(Seq[Any](format.raw/*28.52*/("""
                        """),_display_(/*29.26*/for(product <- products) yield /*29.50*/ {_display_(Seq[Any](format.raw/*29.52*/("""
                            """),_display_(/*30.30*/if(warehouse.id == product.warehouse)/*30.67*/ {_display_(Seq[Any](format.raw/*30.69*/("""
                                """),_display_(/*31.34*/if(product.stock != 0)/*31.56*/ {_display_(Seq[Any](format.raw/*31.58*/("""
                                    """),format.raw/*32.37*/("""<tr>
                                        <th scope="row">"""),_display_(/*33.58*/product/*33.65*/.code),format.raw/*33.70*/("""</th>
                                        <td>"""),_display_(/*34.46*/product/*34.53*/.name),format.raw/*34.58*/("""</td>
                                        <td>"""),_display_(/*35.46*/product/*35.53*/.stock),format.raw/*35.59*/("""</td>
                                        <td>"""),_display_(/*36.46*/warehouse/*36.55*/.id),format.raw/*36.58*/(""" """),format.raw/*36.59*/("""- """),_display_(/*36.62*/warehouse/*36.71*/.name),format.raw/*36.76*/("""</td>
                                        <td>"""),_display_(/*37.46*/product/*37.53*/.date.format("dd/MM/YYYY - HH:mm")),format.raw/*37.87*/("""</td>

                                    </tr>
                                """)))}),format.raw/*40.34*/("""
                            """)))}),format.raw/*41.30*/("""
                        """)))}),format.raw/*42.26*/("""
                    """)))}),format.raw/*43.22*/("""

                    """),format.raw/*45.21*/("""</tbody>
                </table>
            """)))}/*47.15*/else/*47.20*/{_display_(Seq[Any](format.raw/*47.21*/("""
                """),format.raw/*48.17*/("""<div class="card-header d-flex align-items-center">

                    <h3 class="h4">No existe inventario en ninguna bodega</h3>
                </div>

            """)))}),format.raw/*53.14*/("""
            """),format.raw/*54.13*/("""</div>
        </div>
    </div>
  <!--  web: target/universal/stage/bin/paperweb -Dhttp.port=$PORT $PLAY_OPTS-->

</div>






""")))}))
      }
    }
  }

  def render(products:List[Product],warehouses:List[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(products,warehouses)

  def f:((List[Product],List[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (products,warehouses) => apply(products,warehouses)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 19:55:15 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/inventory.scala.html
                  HASH: 3679d9c9ff3c9c2455483bbd7d917c82378f6d97
                  MATRIX: 975->1|1126->57|1155->61|1182->80|1220->81|1247->82|1418->227|1450->251|1489->253|1535->271|2254->963|2298->991|2338->993|2391->1019|2431->1043|2471->1045|2528->1075|2574->1112|2614->1114|2675->1148|2706->1170|2746->1172|2811->1209|2900->1271|2916->1278|2942->1283|3020->1334|3036->1341|3062->1346|3140->1397|3156->1404|3183->1410|3261->1461|3279->1470|3303->1473|3332->1474|3362->1477|3380->1486|3406->1491|3484->1542|3500->1549|3555->1583|3668->1665|3729->1695|3786->1721|3839->1743|3889->1765|3955->1813|3968->1818|4007->1819|4052->1836|4252->2005|4293->2018
                  LINES: 28->1|33->1|36->4|36->4|36->4|37->5|41->9|41->9|41->9|43->11|60->28|60->28|60->28|61->29|61->29|61->29|62->30|62->30|62->30|63->31|63->31|63->31|64->32|65->33|65->33|65->33|66->34|66->34|66->34|67->35|67->35|67->35|68->36|68->36|68->36|68->36|68->36|68->36|68->36|69->37|69->37|69->37|72->40|73->41|74->42|75->43|77->45|79->47|79->47|79->47|80->48|85->53|86->54
                  -- GENERATED --
              */
          