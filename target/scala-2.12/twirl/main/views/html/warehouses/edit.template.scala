
package views.html.warehouses

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object edit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(wareHouseForm: Form[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.34*/("""

"""),_display_(/*3.2*/layout("Editar Bodega")/*3.25*/{_display_(Seq[Any](format.raw/*3.26*/("""
    """),format.raw/*4.5*/("""<h1>Editar Bodega</h1>

    <div class="col-lg-12">
        <div class="block">

            <div class="block-body">
            """),_display_(/*10.14*/helper/*10.20*/.form(action= routes.WarehousesController.update() )/*10.72*/{_display_(Seq[Any](format.raw/*10.73*/("""
                """),format.raw/*11.17*/("""<div class="form-group row">
                    <label class="col-sm-3 form-control-label">Id</label>
                    <div class="col-sm-9">
                    """),_display_(/*14.22*/helper/*14.28*/.inputText(wareHouseForm("id"),'class->"form-control",'_label ->"",'placeholder->"id",'readonly->"readonly")),format.raw/*14.136*/("""
                    """),format.raw/*15.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Nombre</label>
                    <div class="col-sm-9">
                    """),_display_(/*20.22*/helper/*20.28*/.inputText(wareHouseForm("name"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Nombre",'required->"true" )),format.raw/*20.160*/("""
                    """),format.raw/*21.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Direccion </label>
                    <div class="col-sm-9">
                    """),_display_(/*26.22*/helper/*26.28*/.inputText(wareHouseForm("address"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Direccion",'required->"true" )),format.raw/*26.166*/("""
                    """),format.raw/*27.21*/("""</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Encargado</label>
                    <div class="col-sm-9">
                    """),_display_(/*32.22*/helper/*32.28*/.inputText(wareHouseForm("admin"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Encargado",'required->"true" )),format.raw/*32.164*/("""

                    """),format.raw/*34.21*/("""</div>
                </div>
                <button type="submit" class="btn btn-success btn-block">Editar</button>


            """)))}),format.raw/*39.14*/("""
            """),format.raw/*40.13*/("""</div>
        </div>
    </div>

""")))}))
      }
    }
  }

  def render(wareHouseForm:Form[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(wareHouseForm)

  def f:((Form[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (wareHouseForm) => apply(wareHouseForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 20:00:02 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/warehouses/edit.scala.html
                  HASH: 8fc56e168caf5bd3f24290aced967263af68961c
                  MATRIX: 967->1|1094->33|1122->36|1153->59|1191->60|1222->65|1380->196|1395->202|1456->254|1495->255|1540->272|1734->439|1749->445|1879->553|1928->574|2172->791|2187->797|2341->929|2390->950|2638->1171|2653->1177|2813->1315|2862->1336|3109->1556|3124->1562|3282->1698|3332->1720|3496->1853|3537->1866
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|42->10|42->10|42->10|42->10|43->11|46->14|46->14|46->14|47->15|52->20|52->20|52->20|53->21|58->26|58->26|58->26|59->27|64->32|64->32|64->32|66->34|71->39|72->40
                  -- GENERATED --
              */
          