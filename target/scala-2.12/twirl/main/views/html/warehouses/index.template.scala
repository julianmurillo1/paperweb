
package views.html.warehouses

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(warehouses: List[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.31*/("""
    """),_display_(/*2.6*/layout("Bodegas")/*2.23*/ {_display_(Seq[Any](format.raw/*2.25*/("""
        """),format.raw/*3.9*/("""<div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">


                </div>
                <div class="col-lg-12">
                    <div class="block margin-bottom-sm">

                    """),_display_(/*12.22*/if( warehouses.length != 0)/*12.49*/ {_display_(Seq[Any](format.raw/*12.51*/("""
                        """),format.raw/*13.25*/("""<div class="card-header d-flex align-items-center">
                            <h3 class="h4">Bodegas</h3>
                        </div>

                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">Código </th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col">Responsable </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*28.30*/for(warehouse <- warehouses) yield /*28.58*/ {_display_(Seq[Any](format.raw/*28.60*/("""
                                """),format.raw/*29.33*/("""<tr>
                                    <td>"""),_display_(/*30.42*/warehouse/*30.51*/.id),format.raw/*30.54*/("""</td>
                                    <td>"""),_display_(/*31.42*/warehouse/*31.51*/.name),format.raw/*31.56*/("""</td>
                                    <td>"""),_display_(/*32.42*/warehouse/*32.51*/.address),format.raw/*32.59*/("""</td>
                                    <td>"""),_display_(/*33.42*/warehouse/*33.51*/.admin),format.raw/*33.57*/("""</td>
                                    <td><a href="/warehouses/"""),_display_(/*34.63*/warehouse/*34.72*/.id),format.raw/*34.75*/("""" class="btn btn-primary">Inventario </a></td>
                                    <td><a href="/warehouses/edit/"""),_display_(/*35.68*/warehouse/*35.77*/.id),format.raw/*35.80*/("""" class="btn btn-success">Editar </a></td>
                                    <td><a href="/warehouses/delete/"""),_display_(/*36.70*/warehouse/*36.79*/.id),format.raw/*36.82*/("""" class="btn btn-danger">Eliminar </a></td>

                                </tr>
                            """)))}),format.raw/*39.30*/("""

                            """),format.raw/*41.29*/("""</tbody>
                        </table>
                    """)))}/*43.22*/else/*43.27*/{_display_(Seq[Any](format.raw/*43.28*/("""

                        """),format.raw/*45.25*/("""<div class="card-header d-flex align-items-center">

                            <h3 class="h4">No existen bodegas creadas </h3>
                        </div>
                    """)))}),format.raw/*49.22*/("""

                    """),format.raw/*51.21*/("""</div>


                    </div>
                </div>
            </div>



    """)))}))
      }
    }
  }

  def render(warehouses:List[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(warehouses)

  def f:((List[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (warehouses) => apply(warehouses)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 19:57:04 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/warehouses/index.scala.html
                  HASH: b0d07fcb74f14b7251e867aa414803706da4d8fe
                  MATRIX: 968->1|1092->30|1123->36|1148->53|1187->55|1222->64|1492->307|1528->334|1568->336|1621->361|2402->1115|2446->1143|2486->1145|2547->1178|2620->1224|2638->1233|2662->1236|2736->1283|2754->1292|2780->1297|2854->1344|2872->1353|2901->1361|2975->1408|2993->1417|3020->1423|3115->1491|3133->1500|3157->1503|3298->1617|3316->1626|3340->1629|3479->1741|3497->1750|3521->1753|3664->1865|3722->1895|3804->1958|3817->1963|3856->1964|3910->1990|4122->2171|4172->2193
                  LINES: 28->1|33->1|34->2|34->2|34->2|35->3|44->12|44->12|44->12|45->13|60->28|60->28|60->28|61->29|62->30|62->30|62->30|63->31|63->31|63->31|64->32|64->32|64->32|65->33|65->33|65->33|66->34|66->34|66->34|67->35|67->35|67->35|68->36|68->36|68->36|71->39|73->41|75->43|75->43|75->43|77->45|81->49|83->51
                  -- GENERATED --
              */
          