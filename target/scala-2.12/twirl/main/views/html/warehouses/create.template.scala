
package views.html.warehouses

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object create extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Warehouse],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(wareHouseForm: Form[Warehouse]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.34*/("""

"""),_display_(/*3.2*/layout("Crear Bodega")/*3.24*/{_display_(Seq[Any](format.raw/*3.25*/("""
    """),format.raw/*4.5*/("""<h1>Crear Nueva Bodega</h1>

    <div class="col-lg-12">
        <div class="block">

            <div class="block-body">
    """),_display_(/*10.6*/helper/*10.12*/.form(action= routes.WarehousesController.save() )/*10.62*/{_display_(Seq[Any](format.raw/*10.63*/("""
        """),format.raw/*11.9*/("""<div class="form-group row">
            <label class="col-sm-3 form-control-label">Nombre</label>
            <div class="col-sm-9">
            """),_display_(/*14.14*/helper/*14.20*/.inputText(wareHouseForm("name"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Nombre",'required->"true" )),format.raw/*14.152*/("""
            """),format.raw/*15.13*/("""</div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Direccion </label>
            <div class="col-sm-9">
            """),_display_(/*20.14*/helper/*20.20*/.inputText(wareHouseForm("address"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Direccion",'required->"true" )),format.raw/*20.158*/("""
            """),format.raw/*21.13*/("""</div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Encargado</label>
            <div class="col-sm-9">
            """),_display_(/*26.14*/helper/*26.20*/.inputText(wareHouseForm("admin"),'class->"form-control form-control-success",'_label ->"",'placeholder->"Encargado",'required->"true" )),format.raw/*26.156*/("""

            """),format.raw/*28.13*/("""</div>
        </div>
        <button type="submit" class="btn btn-success btn-block">Crear</button>


    """)))}),format.raw/*33.6*/("""
            """),format.raw/*34.13*/("""</div>
        </div>
    </div>

""")))}))
      }
    }
  }

  def render(wareHouseForm:Form[Warehouse]): play.twirl.api.HtmlFormat.Appendable = apply(wareHouseForm)

  def f:((Form[Warehouse]) => play.twirl.api.HtmlFormat.Appendable) = (wareHouseForm) => apply(wareHouseForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 20:00:02 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/warehouses/create.scala.html
                  HASH: f9c1365943f44dab091fa85c1e633b35ded652c3
                  MATRIX: 969->1|1096->33|1124->36|1154->58|1192->59|1223->64|1377->192|1392->198|1451->248|1490->249|1526->258|1700->405|1715->411|1869->543|1910->556|2118->737|2133->743|2293->881|2334->894|2541->1074|2556->1080|2714->1216|2756->1230|2894->1338|2935->1351
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|42->10|42->10|42->10|42->10|43->11|46->14|46->14|46->14|47->15|52->20|52->20|52->20|53->21|58->26|58->26|58->26|60->28|65->33|66->34
                  -- GENERATED --
              */
          