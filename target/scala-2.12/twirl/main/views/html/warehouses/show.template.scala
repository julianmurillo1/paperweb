
package views.html.warehouses

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object show extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[List[Product],Warehouse,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(  products : List[Product], warehouse: Warehouse ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.53*/("""
"""),_display_(/*2.2*/layout("Bodegas")/*2.19*/ {_display_(Seq[Any](format.raw/*2.21*/("""



    """),format.raw/*6.5*/("""<h1 class=""> Inventario en : """),_display_(/*6.36*/warehouse/*6.45*/.name),format.raw/*6.50*/(""" """),format.raw/*6.51*/("""</h1>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">Código de venta producto</th>
                <th scope="col">Nombre producto</th>
                <th scope="col">Numero de unidades en bodega</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

        """),_display_(/*18.10*/for(product <- products) yield /*18.34*/{_display_(Seq[Any](format.raw/*18.35*/("""
         """),_display_(/*19.11*/if(warehouse.id == product.warehouse)/*19.48*/ {_display_(Seq[Any](format.raw/*19.50*/("""

             """),format.raw/*21.14*/("""<tr>
                 <td>"""),_display_(/*22.23*/product/*22.30*/.code),format.raw/*22.35*/("""</td>
                 <td>"""),_display_(/*23.23*/product/*23.30*/.name),format.raw/*23.35*/("""</td>
                 <td>"""),_display_(/*24.23*/product/*24.30*/.stock),format.raw/*24.36*/("""</td>
                 <td><a href="/products/edit/"""),_display_(/*25.47*/product/*25.54*/.id),format.raw/*25.57*/("""" class="btn btn-primary">Editar numero en stock </a></td>

             </tr>

""")))})))}),format.raw/*29.3*/("""
        """),format.raw/*30.9*/("""</tbody>
    </table>


""")))}),format.raw/*34.2*/("""











"""))
      }
    }
  }

  def render(products:List[Product],warehouse:Warehouse): play.twirl.api.HtmlFormat.Appendable = apply(products,warehouse)

  def f:((List[Product],Warehouse) => play.twirl.api.HtmlFormat.Appendable) = (products,warehouse) => apply(products,warehouse)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Dec 01 19:26:19 COT 2017
                  SOURCE: /Users/julianmurillo/Documents/programming/PlayFramework/paperweb/app/views/warehouses/show.scala.html
                  HASH: 010ce7282a0f0e909e7e97c329dccc08fcb80676
                  MATRIX: 975->1|1121->52|1148->54|1173->71|1212->73|1246->81|1303->112|1320->121|1345->126|1373->127|1759->486|1799->510|1838->511|1876->522|1922->559|1962->561|2005->576|2059->603|2075->610|2101->615|2156->643|2172->650|2198->655|2253->683|2269->690|2296->696|2375->748|2391->755|2415->758|2530->840|2566->849|2621->874
                  LINES: 28->1|33->1|34->2|34->2|34->2|38->6|38->6|38->6|38->6|38->6|50->18|50->18|50->18|51->19|51->19|51->19|53->21|54->22|54->22|54->22|55->23|55->23|55->23|56->24|56->24|56->24|57->25|57->25|57->25|61->29|62->30|66->34
                  -- GENERATED --
              */
          