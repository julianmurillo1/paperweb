# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table product (
  id                            serial not null,
  code                          integer,
  name                          varchar(255),
  brand                         varchar(255),
  provider                      varchar(255),
  stock                         integer,
  image                         varchar(255),
  date                          timestamptz,
  warehouse                     integer,
  constraint pk_product primary key (id)
);

create table warehouse (
  id                            serial not null,
  name                          varchar(255),
  address                       varchar(255),
  admin                         varchar(255),
  constraint pk_warehouse primary key (id)
);


# --- !Downs

drop table if exists product cascade;

drop table if exists warehouse cascade;

