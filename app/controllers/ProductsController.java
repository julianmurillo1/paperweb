package controllers;

import models.Product;
import models.Warehouse;
import play.api.Mode;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.*;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.Random;

import views.html.products.*;

public class ProductsController  extends Controller {


    @Inject
    FormFactory formFactory;


    /**Lista los productos desde el modelo y los renderiza en la pagina principal
    *@author julian
    *
     */
    public Result index() {
        List<Product> products = Product.find.all();
        return ok(views.html.products.index.render(products));
    }


    /**Trae  productos desde el modelo y los renderiza junto con el modelo de bodegas
     *@author julian
     *
     */
    public Result inventory() {
        List<Product> products = Product.find.all();
        List<Warehouse> warehouses = Warehouse.find.all();

        return ok(views.html.inventory.render(products , warehouses));

    }


    /**Renderiza formulario de productos
     *@author julian
     *
     */
    public Result create(){
        Form<Product> productForm = formFactory.form(Product.class);
        List<Warehouse> warehouses = Warehouse.find.all();

        return ok(views.html.products.create.render( productForm, warehouses));
    }

    /**Guarda los productos modelados en la clase producto tomando los datos que recibe por Post
     *@author julian
     *
     */
    @BodyParser.Of(MyMultipartFormDataBodyParser.class)
    public Result save()  throws IOException {
        Form<Product> ProductForm = formFactory.form(Product.class).bindFromRequest();
        Product product = ProductForm.get();
        final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("file");
        final File file = filePart.getFile();
        File newFile = new File ("./public/images/products", product.code +".jpg");
        file.renameTo(newFile);

        String imageName= product.code+".jpg";

        product.date =new Date();
        product.image= imageName;
        product.save();
        return redirect(routes.ProductsController.index());

    }


    private long operateOnTempFile(File file) throws IOException {
        final long size = Files.size(file.toPath());
        Files.deleteIfExists(file.toPath());
        return size;
    }
    /**Renderiza el producto individualmente según la busqueda por ID
     *@author julian
     *
     */

    public Result show(Integer id){

        Product product = Product.find.byId(id);
        if(product==null){
            return notFound("no se encontro ningún producto");
        }

        return ok(show.render(product));
    }

    /**Crear formulario y lo renderiza en la pagina de edicion
     *@author julian
     *
     */


    public Result edit(Integer id){
        Product product = Product.find.byId(id);
        Form<Product> productForm = formFactory.form(Product.class).fill(product);
        List<Warehouse> warehouses = Warehouse.find.all();
        return ok(views.html.products.edit.render(productForm, warehouses));
    }


    /**Edita el producto segun la Id enviada por Post
     *@author julian
     *
     */
    @BodyParser.Of(MyMultipartFormDataBodyParser.class)
    public  Result update()throws IOException{

        Product product = formFactory.form(Product.class).bindFromRequest().get();
        Product pProduct = Product.find.byId(product.id);

        final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("file");
        final File file = filePart.getFile();
        Random rnd = new Random();
        Integer numero = (int) (Math.random() * 99999999) + 1;


        File newFile = new File ("./public/images/products", numero +".jpg");
        file.renameTo(newFile);

        String imageName;
        imageName=numero +".jpg" ;


        if (pProduct==null){
            return notFound("Product not found");

        }

        pProduct.code = product.code;
        pProduct.name = product.name;
        pProduct.brand= product.brand;
        pProduct.provider= product.provider;
        pProduct.stock= product.stock;
        pProduct.date = new Date();
        pProduct.image = imageName;
        pProduct.warehouse= product.warehouse;
        pProduct.update();
        return redirect(routes.ProductsController.index());
    }



    /**Elimina un producto  según la busqueda por ID
     *@author julian
     *
     */

    public Result destroy(Integer id){
        Product product = Product.find.byId(id);
        if(product==null){
            return notFound("no se encontro el Producto");

        }
        product.delete();
        return redirect(routes.ProductsController.index());

    }



}
