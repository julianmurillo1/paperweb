package controllers;

import models.Product;
import models.Warehouse;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.products.show;
import  views.html.warehouses.*;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class WarehousesController extends Controller {


    @Inject
    FormFactory formFactory;
    public Result index() {
        List<Warehouse> warehouses = Warehouse.find.all();
        return ok(views.html.warehouses.index.render(warehouses));
    }

    public Result show(Integer id){

        Warehouse warehouse = Warehouse.find.byId(id);
        List<Product> products = Product.find.all();
        if(warehouse==null){
            return notFound("no se encontro ningúna bodega ");
        }

        return ok(views.html.warehouses.show.render( products, warehouse));
    }



    public Result create(){
        Form<Warehouse> warehouseForm = formFactory.form(Warehouse.class);

        return ok(views.html.warehouses.create.render( warehouseForm));
    }




    //metodo que guarda las bodegas

    public Result save()   {
        Form<Warehouse> wareHouseForm = formFactory.form(Warehouse.class).bindFromRequest();
        Warehouse warehouse = wareHouseForm.get();
        warehouse.save();
        return redirect(routes.WarehousesController.index());

    }

    public Result edit(Integer id){
        Warehouse warehouse = Warehouse.find.byId(id);
        Form<Warehouse> wareHouseForm = formFactory.form(Warehouse.class).fill(warehouse);
        List<Warehouse> warehouses = Warehouse.find.all();
        return ok(views.html.warehouses.edit.render(wareHouseForm) );
    }

    public  Result update(){

        Warehouse warehouse = formFactory.form(Warehouse.class).bindFromRequest().get();
        Warehouse pWarehouse = Warehouse.find.byId(warehouse.id);


        if (pWarehouse==null){
            return notFound("Warehouse not found");

        }
        pWarehouse.id= warehouse.id;
        pWarehouse.name = warehouse.name;
        pWarehouse.address = warehouse.address;
        pWarehouse.admin = warehouse.admin;
        pWarehouse.update();
        return redirect(routes.WarehousesController.index());
    }


    public Result destroy(Integer id){
        Warehouse warehouse = Warehouse.find.byId(id);
        if(warehouse==null){
            return notFound("no se encontro la bodega");

        }
        warehouse.delete();
        return redirect(routes.WarehousesController.index());

    }


}
