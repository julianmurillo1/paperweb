package models;
import io.ebean.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;


@Entity
public class Product extends Model{
    @Id
    public Integer id;
    public Integer code;
    public String name;
    public String brand;
    public String provider;
    public Integer stock;
    public String image;
    public Date   date;
    public Integer warehouse  ;

    public static Finder<Integer, Product> find = new Finder<>(Product.class);

}
