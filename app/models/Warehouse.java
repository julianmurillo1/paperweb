package models;
import io.ebean.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Warehouse extends  Model {
    @Id
    public Integer id;
    public String name;
    public String address;
    public String admin;


    public static Finder<Integer, Warehouse> find = new Finder<>(Warehouse.class);

}
