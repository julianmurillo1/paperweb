// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.7")
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.2.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "4.0.6")
addSbtPlugin("com.heroku" % "sbt-heroku" % "2.0.0")